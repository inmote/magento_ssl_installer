#!/bin/bash
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi


function process() {
  # export variables to use these in docker-container
  exportDataForDocker

  # install Docker
  installDockerCE

  #install composer
  installComposer

  #clone the repo and install magento
  cloneAndBuildDocker
}

#docker exec -ti apache2 bash

#install_magento

## Get input from User
function getUserInput {
  printf "\nFor the next steps some information has to be collected, \nplease confirm with Enter when you are ready.\n\n"
  read -n 1 -s -r

  printf "\nFor the required repo we need a password, which must be entered for security reasons\n"
  read -p "Bitbucket Repo password: " bitbucket_password

  printf "\n\nCollect Magneto informations\n\n"
  read -p "Magento IP-Address or URL: " magento_ip_uri
  read -p "Magento Admin Username: " magneto_username
  read -s -p "Magento Admin Password: " magento_password

  printf "\n\nCollect Database information\n"
  read -p "Database Name: " database_name
  read -p "Database User: " database_user
  read -s -p "Database Password: " database_password
}

function installDockerCE {
  printf "\n**************************************
    **      Install Docker CE           **
    **************************************\n\n"
  apt-get remove docker docker-engine docker.io
  apt-get update
  apt-get install apt-transport-https ca-certificates curl software-properties-common -y
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  apt-key fingerprint 0EBFCD88
  add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  apt-get update
  apt-get install docker-ce git sshpass -y
}

function installComposer {
  curl -L "https://github.com/docker/compose/releases/download/1.21.0-rc1/docker-compose-$(uname -s)-$(uname -m)" > ./docker-compose
  mv ./docker-compose /usr/bin/docker-compose
  chmod +x /usr/bin/docker-compose
}

function cloneAndBuildDocker {
#/root/.composer/auth.json'

  cd ~/ 

  #git clone https://chriss1983:$bitbucket_password@bitbucket.org/chriss1983projecs/magento2ssl.git
  git clone https://redeyedex:$bitbucket_password@bitbucket.org/redeyedex/magento2_ssl.git

  cd magento2ssl

  database_user_password=`date | md5sum | fold -w 12 | head -n 1`

  # change ip in ssl_server/default
  sed -i "s/IPADDRESS/${magento_ip_uri}/g" ssl_server/default

  # change data for database in database_server/mysql.sh
  sed -i "s/DATABASENAME/${database_name}/g" database_server/mysql.sh
  sed -i "s/DATABASEUSER/${database_user}/g" database_server/mysql.sh
  sed -i "s/DATABASEROOT/${database_password}/g" database_server/mysql.sh

  sed -i "s/DATABASEUSERPASSWORD/${database_user_password}/g" database_server/mysql.sh

  # change data for database in docker-compose
  sed -i "s/DBNAME/${database_name}/g" docker-compose.yml
  sed -i "s/DBUSER/${database_user}/g" docker-compose.yml
  sed -i "s/DBPASS/${database_password}/g" docker-compose.yml
  sed -i "s/MAGENTOUSER/${magneto_username}/g" docker-compose.yml
  sed -i "s/MAGNETOPASS/${magento_password}/g" docker-compose.yml

  # change data for database in magento2/install_magneto.sh
  sed -i "s/DATABASENAME/${database_name}/g" magento2/install_magneto.sh
  sed -i "s/DATABASEUSER/${database_user}/g" magento2/install_magneto.sh
  sed -i "s/DATABASEROOT/${database_user_password}/g" magento2/install_magneto.sh
  sed -i "s/MAGENTOUSER/${magento_user}/g" magento2/install_magneto.sh
  sed -i "s/MAGNETOPASS/${magento_password}/g" magento2/install_magneto.sh

  docker-compose build

  docker-compose up -d

  docker exec -i mysql cat /var/log/check.log

  read -n 1 -s -r -p "Bitte notieren Sie sich die Zugangsdaten über diese Zeile!
  otherwise you can get these infos later by enter:
  docker exec -i mysql cat /var/log/check.log
  press enter when you ready . . "
}

function exportDataForDocker {
  export MAGENTO_IP_URI=${magento_ip_uri}
  export MAGNETO_USERNAME=${magneto_username}
  export MAGNETO_PASSWORD=${magento_password}

  export DB_NAME=${database_name}
  export DB_USER=${database_user}
  export DB_PASSWORD=${database_password}
}

clear

printf "\n\n
***************************************
** Welcome to Magento auto Installer **
***************************************\n\n"

getUserInput

read -p "are the informations correct? (y/n)?" choice
case "$choice" in 
  y|Y ) process;;
  n|N ) getUserInput;;
  * ) echo "invalid";;
esac

